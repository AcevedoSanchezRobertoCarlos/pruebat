<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actualiza</title>
</head>
<body>
    <h1>Editar usuario</h1>
    <form action="" method="post">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6">
			    <div class="form-group">
                    <label for="nombre" >Nombre Completo:</label>
                    <input type="text" name="nombre" id="nombre">
                </div>
            </div>
                <br>
            <div class="col-12 col-md-6">
			    <div class="form-group">
                <label for="usuario">Nombre Usuario:</label>
                <input type="text" name="usuario" id="usuario">
                </div>
            </div>
                <br>
            <div class="col-12 col-md-6">
			    <div class="form-group">
                <label for="contra">Contraseña:</label>
                <input type="password" name="contra" id="contra">
                </div>
            </div>
                <br>
            <div class="col-12 col-md-6">
			    <div class="form-group">
                <button type="submit" class="btn btn-raised btn-primary btn-sm">GUARDAR</button>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>