<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>listar</title>
</head>
<body>
    <div class="container-fluid">
		<div class="table-responsive">
            <table class="table table-dark table-sm" id="tablausuario" style="margin-left:auto;margin-right:auto;">
                <thead>
				    <tr class="text-center">
						<th>NOMBRE COMPLETO</th>
						<th>USUARIO</th>
						<th>EDITAR</th>
						<th>ELIMINAR</th>
					</tr>
				</thead>
                <tbody>
                    <tr class="text-center" >
                        <td>Roberto Carlos Acevedo Sanchez</td>
                        <td>RobertoCar</td>
                        <td><button>Editar</button></td>
                        <td><button>Eliminar</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>